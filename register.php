<?php session_start(); ?>
<!doctype html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
     <link rel="stylesheet" href="css/login.css">
    <title>СОКРАТ</title>
  </head>
  <body>
    <br>

    <form action="signup.php" method="post" enctype="multipart/form-data">
      <div class="d-flex flex-column align-items-center">
        <label >Имя</label>
        <input type="text" name="nameIn" placeholder="Введите своё имя">
        <label>Логин</label>
        <input type="text" name="loginIn" placeholder="Введите логин">
        <label>Email</label>
        <input type="text" name="emailIn" placeholder="Введите свой email">
        <label>Пароль</label>
        <input type="password" name="passIn1" placeholder="Введите пароль">
        <label>Подтвердите пароль</label>
        <input type="password" name="passIn2" placeholder="Повторите пароль">
        <button>Зарегистрироваться</button>
                <p>У вас уже есть аккаунт? <a href="login.php">Войдите в свой профиль!</a></p>
                <p class="InfoMessage"><?php echo $_SESSION['message']; unset($_SESSION['message']);?> </p>
      </div>
    </form>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

  </body>
</html>
